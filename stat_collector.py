"""
This util runs program specified as first argument as path, see below.
and collects system resources statistics (rss, vms, number of handles).

It runs continuously and prints stats to console
at period specified as second argument (in seconds, see below)
until monitored program will be shut.

After that it saves all collected data to .xls file in util folder.
It accepts alternative path for this files as optional argument (see below).

!!!WARNING!!!
Every run on the same program rewrites previous file due usability reasons.
So feel free to backup important files manually.

Autosave to file runs every 10 minutes, can be set by optional argument (see below).

To call util you need to pass it two required arguments and two optional:
stat_collector.py <Path to monitored program> <Stat update frequency>
-e=<Alternative path to data file> -s=<Autosave period>

<Path to monitored program> (path)
    - path to local file, according to your command line requirments, e.g.
      C:/Windows/System32/notepad.exe       --  for Windows
      /org/apps/mousepad/mousepad-window.c  --  for Linux-based systems

<Stat update frequency>     (seconds)
    - period (in seconds) between requesting processing information

<Alternative path to data file> '-e', '--save_to_file_every'  (path)
    - By default data files saved to same folder where stat_collector runs.
      You can specify alternative path
      to directory where you want to store your data
      same format as for Path parameter

<Autosave period> '-s', '--stat_file_path'  (minutes)
    - Collected data automatically saves to file every 10 minutes by default.
      You can set desired autosave time in minutes
"""


import time
import platform
import argparse
import subprocess
from datetime import datetime, timedelta
import psutil
import pandas as pd
from openpyxl import Workbook
from openpyxl.utils.dataframe import dataframe_to_rows


def get_process_info(process):
    """collects stats using psutil and returns them as dict inside loop"""
    process_stats = {}

    try:
        # retrieve specified information as dictionary
        stat_sict = process.as_dict(
                attrs=['pid', 'name', 'status', 'create_time',
                'memory_info', 'num_handles']
            )
        pid = stat_sict['pid']
        name = stat_sict['name']
        status = stat_sict['status']

        # retrieve process start time as additional stat
        create_time = datetime.fromtimestamp(stat_sict['create_time'])

        # retrieve CPU separately with minimum recommended interval
        # we don't use system-wide CPU utilization, see doc:
        # 'https://psutil.readthedocs.io/en/latest/#psutil.Process.cpu_percent'
        cpu_usage = process.cpu_percent(interval=0.1)

        # number of handles used by this process (Win-based OS) /
        # number of file descriptors opened by process (linux-based OS)
        num_handles = stat_sict['num_handles']

        # Working Set (Win-based OS) / Resident Set Size (linux-based OS)
        memory_usage_rss = stat_sict['memory_info'].rss

        # Private Bytes (Win-based OS) / Virtual Memory Size  (linux-based OS)
        memory_usage_vms = stat_sict['memory_info'].vms
    except psutil.AccessDenied:
        with process.oneshot():
            pid = process.pid
            name = process.name()
            status = process.status()
            cpu_usage = process.cpu_percent(interval=0.1)
        try:
            create_time = datetime.fromtimestamp(process.create_time())
        except OSError:
            create_time = datetime.fromtimestamp(psutil.boot_time())
        num_handles = 0
        memory_usage_rss = 0
        memory_usage_vms = 0

    process_stats.update({
        'pid': pid, 'name': name, 'status': status,
        'create_time': create_time, 'cpu_usage': cpu_usage,
        'rss': memory_usage_rss,
        'vms': memory_usage_vms,
        'number_handles': num_handles
    })

    # print specified stats to console
    print(os_specific_printer(platform.system(), process_stats))

    return process_stats


def os_specific_printer(current_os, stats):
    """prints stats to console depending on OS"""
    if current_os == 'Windows':
        return (f"<------{stats['name']}------>  {freq}s\n"\
                f"CPU usage: {stats['cpu_usage']}\n"
                f"Working set: {get_size(stats['rss'])}\n"\
                f"Private bytes: {get_size(stats['vms'])}\n"\
                f"Number of handles: {stats['number_handles']}\n")
    elif current_os == 'Linux':
        return (f"<------{stats['name']}------>  {freq}s\n"\
                f"CPU usage: {stats['cpu_usage']}\n"
                f"Resident set size: {get_size(stats['rss'])}\n"\
                f"Virtual memory size: {get_size(stats['vms'])}\n"\
                f"Number of file descriptors: {stats['number_handles']}\n")
    else:
        return "Unsupported operation system"


def get_size(mem_bytes):
    """formatting memory bytes to more readable units"""
    for unit in ['', 'K', 'M', 'G', 'T', 'P']:
        if mem_bytes < 1024:
            return f"{mem_bytes:.2f}{unit}B"
        mem_bytes /= 1024


def construct_record(stats):
    """format single stat snapshot for passing it to dataframe"""
    record = pd.Series(stats)
    record['rss'] = get_size(record['rss'])
    record['vms'] = get_size(record['vms'])
    record['create_time'] = record['create_time'].strftime(
                    "%Y-%m-%d %H:%M:%S"
                )
    return record


def save_to_file(stat_file):
    """saves stat containing df to default or specified file"""
    wb = Workbook()
    ws = wb.active
    for r in dataframe_to_rows(df, index=True, header=True):
        ws.append(r)

    if not stat_file:
        wb.save(f'mem_stats_{process_info["name"]}.xlsx')
    else:
        wb.save(stat_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="statcollector",
        description="System resources monitor"
    )
    parser.add_argument('path', help='file path to process to monitor')
    parser.add_argument('update_frequency',
        help='frequency of update process info, 30 secs by default')
    parser.add_argument('-e', '--save_to_file_every', default='10',
        help='how often do you want to save to file, 10 minutes by default')
    parser.add_argument('-s', '--stat_file_path',
        help='file path to your stats sile path, current folder by default')

    # parse arguments
    args = parser.parse_args()
    file_path = args.path
    freq = float(args.update_frequency)
    save_freq = args.save_to_file_every
    stat_file_path = args.stat_file_path

    # DataFrame to contain all collected stats
    df = pd.DataFrame(columns=['pid', 'name', 'status', 'create_time',
            'cpu_usage', 'rss', 'vms', 'number_handles'])
    df.set_index('create_time', inplace=True)

    # initiate process under study
    monitored_process = subprocess.Popen(file_path)
    # timeout for autosave stats to file
    timeout_at = datetime.now() + timedelta(minutes=float(save_freq))

    # start process-lifetime loop printing stats
    while psutil.pid_exists(monitored_process.pid):
        for p in psutil.process_iter():
            # save stat df to file if timeout is reached
            if timeout_at - datetime.now() < timedelta(seconds=0):
                save_to_file(stat_file_path)
                timeout_at = datetime.now() + timedelta(
                    minutes=float(save_freq)
                )
            if p.pid == monitored_process.pid:
                process_info = get_process_info(p)

        df = df.append(construct_record(process_info), ignore_index=True)
        # period between stats collecting
        time.sleep(freq)

    save_to_file(stat_file_path)
